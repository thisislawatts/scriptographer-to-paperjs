/*
 * 
 * Scriptographer into Paper Paths
 * 
 *
 * Based on PaperJS_Export_PathItems from http://DrWoohoo.com, rewritten into Scriptographer for a giggle
 *
 * Exports the Anchor Points of PathItem(s) drawn in Adobe Illustrator in a format that's ready to use in PaperJS. 
 *
 * The objective is to easily create more complex shapes in Illustrator and then convert it to a format that will work within PaperJS.
 *  This is an interim solution until SVG import/export is supported in PaperJS.
 * 
 * Distributed under the MIT license. See http://www.opensource.org/licenses/mit-license.php for details.
 * 
 * 
 *
 *
 */

var docLayer = document.activeLayer;
var aPathItems = docLayer.children;
var newPath = "// Paths for PaperJS \r";
var selectedPathItems = false;

for (var i = aPathItems.length - 1; i >= 0 ; i-- )
{
    var currentPathItem = aPathItems[i];

    if ( currentPathItem.selected )
    {
        // Get the Color of the current PathItem
        var handles = "", points = [];

        // Add our points in shorthand
        for (var j = 0, max = currentPathItem.segments.length; j < max; j++)
        {
            // Get the Points along the PathItem
            var newPathPt = currentPathItem.segments[j];

						points[j] = newPathPt.point;            

            // Adding curves only if  handles differ in location to point
            if ( newPathPt.point !== newPathPt.handleIn || newPathPt.point !== newPathPt.handleOut )
						{
                handles +=  "\tpath_" + i + ".segments["+j+"].handleIn ="+ ( newPathPt.handleIn ) +";\r";
                handles +=  "\tpath_" + i + ".segments["+j+"].handleOut =" + ( newPathPt.handleOut ) +";\r";
            }
        }

        newPath +=  "var path_" + i +" = new Path(" + points + ");"    +"\r";
				if ( currentPathItem.fillColor !== null  )
				{
	        newPath += "\tpath_"+i +".fillColor = " + currentPathItem.fillColor + ";\r";
				}
        
				if ( currentPathItem.strokeColor !== null )
				{
					newPath += "\tpath_"+i +".strokeColor = " + currentPathItem.strokeColor + ";\r";
				}
      
  				newPath += "\tpath_"+i +".selected = true;\r";

        if ( currentPathItem.closed) {
            newPath += "\tpath_"+i+".closed = true;\r";   // Select it for easy point location
        }
              
        newPath += handles;

//  Switched off a couple of times because layers are throwing an Error
//       newPath += "var placedSymbol_"+i+" = new PlacedSymbol(path_"+i+");"+"\r"+
//                           "\tlayer.insertChild(0, placedSymbol_"+i+");"+"\r\r";
        selectedPathItems = true;
    }
}

if (selectedPathItems) 
{
   copyToClipboard(newPath );
} else {
    console.log("Select a PathItem on the ArtBoard");
}

function copyToClipboard (text ) {
	console.log(text);
	return;
	
  //   document.layers.add().textFrames.add().contents = text; 
  //  document.layers[0].hasSelectedArtwork = true;
  // app.copy();
  // document.layers[0].remove();
}